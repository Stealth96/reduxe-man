import React from 'react'
import {BrowserRouter, Route,Switch} from 'react-router-dom'
import Home from "./views/Home";
import NavBar from './views/NavBar';

const App = (props:any) => {
  // console.log("props  : ",props);
  return (
    <BrowserRouter>
      <NavBar />
      <Switch>
        <Route path="/" exact component={Home} />
      </Switch>
    </BrowserRouter>
  )
}

export default App
