import * as React from 'react';
import { useSelector } from 'react-redux';

export default () => {
  const bgColor = useSelector((state: ReturnType<any>) => state.backGroundColor);
  return <nav style={{ backgroundColor: bgColor ? "red" : "green" }} >nav</nav>;
};