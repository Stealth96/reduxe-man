import React,{useState,useEffect} from 'react'
import { useDispatch,useSelector } from "react-redux";
import {addToList,checkItem,removeItem} from "./../../action";

function Home() {
  const toDoList = useSelector((state: ReturnType<any>) => state.toDos);
  const dispatch = useDispatch();
  const [list,setList]=useState(toDoList);

  useEffect(()=>{
    setList(toDoList);
  },[toDoList])

  return (
    <>
    <ul>
      {list.map((value:any,index:number)=><li style={{textDecoration:value.tick ?'line-through':'none'}} onDoubleClick={()=>dispatch(removeItem(index))} onClick={()=>dispatch(checkItem(index)) }>{value.text}</li>)}
    </ul>
      <input type="text" placeholder="type todo" onBlur={(e)=>{dispatch(addToList(e.target.value))}} />
    </>
  )
}

export default Home;
