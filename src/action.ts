export const changeTheme = () => ({
  type: "CHANGE_THEME_COLOR"
});

export const addToList = (value:string) => ({
  type: "ADD_TO_LIST",
  value
});

export const checkItem = (index:number) => ({
  type: "CHECK_ITEM",
  index
});

export const removeItem = (index:number) => ({
  type: "REMOVE_ITEM",
  index:index
});