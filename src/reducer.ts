const defaultToDo = [
  {
    text: "kill my self",
    tick: true
  },
  {
    text: "clean house",
    tick: false
  }
]
export default (state: any = { toDos: defaultToDo }, action: any) => {
  switch (action.type) {
    case "ADD_TO_LIST": {
      return {
        ...state,
        toDos: [...state.toDos, { text: action.value, tick: false }]
      }
    }
    case "CHECK_ITEM": {
      return {
        ...state,
        toDos: state.toDos.map((value: any, index: number) => index !== action.index ? value : { text: value.text, tick: !value.tick })
      }
    }
    case "REMOVE_ITEM": {
      return {
        ...state,
        toDos: state.toDos.filter((_value: any, index: number) => index !== action.index)
      }
    }
    default:
      return state;
  }
}