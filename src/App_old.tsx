
import React from 'react';
import { connect } from "react-redux";
import { changeTheme } from "./action";
import { Button, TextField } from '@material-ui/core';
import './style.scss';

// type AppProps = {}
type AppState = {
  input: number,
  operator: null | string,
  oldInput: number | null
  bgColor: boolean
}
class App_old extends React.Component<any, AppState> {
  constructor(props: any) {
    super(props);
    this.state = {
      input: 0,
      operator: null,
      oldInput: null,
      bgColor:true
    };
  }

  changeInput = (inputKey: string | number) => {
    if (typeof inputKey === "number") {
      if (this.state.input === 0) {
        this.setState({ input: parseInt(inputKey.toString()) });
      }
      else {
        this.setState({ input: parseInt(this.state.input.toString() + inputKey.toString()) });
      }
    } else {

      if (/([*|+|-|/])/g.test(inputKey)) {
        this.setState({ operator: inputKey, oldInput: this.state.input, input: 0 });
      } else if (inputKey === "=") {
        if (this.state.operator !== null && this.state.oldInput !== null && this.state.input !== null) {
          // eslint-disable-next-line
          this.setState({ input: parseFloat(eval(`${this.state.oldInput} ${this.state.operator} ${this.state.input}`)) });
        }
      }else if(inputKey=== "CE"){
        this.setState({operator:null,oldInput:null,input:0})
      }

      // switch (inputKey) {
      //   case "+":
      //     this.setState({ operator: inputKey, oldInput: this.state.input, input: 0 });
      //     break;
      //   case "-":
      //     this.setState({ operator: inputKey, oldInput: this.state.input, input: 0 });
      //     break;
      //   case "*":
      //     this.setState({ operator: inputKey, oldInput: this.state.input, input: 0 });
      //     break;
      //   case "/":
      //     this.setState({ operator: inputKey, oldInput: this.state.input, input: 0 });
      //     break;
      //   case "=":
      //     if (this.state.operator !== null && this.state.oldInput !== null && this.state.input !== null) {
      //       // eslint-disable-next-line
      //       this.setState({ input: parseFloat(eval(`${this.state.oldInput} ${this.state.operator} ${this.state.input}`)) });
      //     }
      //     break;

      //   default:
      //     break;
      // }
    }
  }

  UNSAFE_componentWillReceiveProps(nextProps:any){
    if (this.props.backGroundColor!== nextProps.backgroundColor) {
      this.setState({bgColor:!this.state.bgColor})
    }
    
  }

  render() {
    return (
      <div style={{backgroundColor:this.state.bgColor ?"red" :"blue"}} className="grid-container">
        <TextField
          id="outlined-number"
          label="Number"
          type="number"
          value={this.state.input}
          InputLabelProps={{
            shrink: true,
          }}
          style={{ gridColumn: ' 1 / span 4' }}
          variant="outlined"
        />
        <Button onClick={() => this.changeInput(7)} variant="outlined" color="primary">7</Button>
        <Button onClick={() => this.changeInput(8)} variant="outlined" color="primary">8</Button>
        <Button onClick={() => this.changeInput(9)} variant="outlined" color="primary">9</Button>
        <Button onClick={() => this.changeInput("/")} variant="outlined" color="secondary">/</Button>
        <Button onClick={() => this.changeInput(4)} variant="outlined" color="primary">4</Button>
        <Button onClick={() => this.changeInput(5)} variant="outlined" color="primary">5</Button>
        <Button onClick={() => this.changeInput(6)} variant="outlined" color="primary">6</Button>
        <Button onClick={() => this.changeInput("*")} variant="outlined" color="secondary">*</Button>
        <Button onClick={() => this.changeInput(1)} variant="outlined" color="primary">1</Button>
        <Button onClick={() => this.changeInput(2)} variant="outlined" color="primary">2</Button>
        <Button onClick={() => this.changeInput(3)} variant="outlined" color="primary">3</Button>
        <Button onClick={() => this.changeInput("-")} variant="outlined" color="secondary">-</Button>
        <Button onClick={() => this.changeInput(0)} variant="outlined" color="primary">0</Button>
        <Button variant="outlined" color="primary">.</Button>
        <Button onClick={() => this.changeInput("=")} variant="outlined" color="secondary">=</Button>
        <Button onClick={() => this.changeInput("+")} variant="outlined" color="secondary">+</Button>
        <Button onClick={() => this.changeInput("CE")} variant="outlined" color="secondary">CE</Button>
        <Button onClick={() => this.props.changeThemeColor()} variant="outlined" color="secondary">stfu</Button>
      </div>
    );
  }
}

const mapStateToProps = (state : any) => ({
  backGroundColor:state.backGroundColor
})

const mapDispatchToProps = (dispatch : any) => ({
  changeThemeColor:()=>dispatch(changeTheme())
})

export default connect(mapStateToProps,mapDispatchToProps)(App_old);